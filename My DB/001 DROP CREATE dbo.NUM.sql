USE JMTW
GO

/* create and populate a simple numbers table */

IF OBJECT_ID('dbo.NUM') IS NULL
CREATE TABLE dbo.NUM (n INT);

IF NOT EXISTS (SELECT * FROM dbo.NUM)
INSERT dbo.NUM
SELECT TOP 10000 row_number() over(order by t1.number) as N
FROM master..spt_values t1 
    CROSS JOIN master..spt_values t2;





